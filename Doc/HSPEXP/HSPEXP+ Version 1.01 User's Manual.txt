<p align="right">May 2015</p>
<center><h>HSPEXP+ Version 1.01 User's Manual</h></center>
<br>
<center><p>Anurag Mishra<br>
Paul B. Duda<br>
Mark Gray<br>
Brian R. Bicknell<br>
Anthony S. Donigian, Jr.<br>
Rebecca Zeckoski*</p></center>
<br>
<center><p>AQUA TERRA Consultants<br>
Mountain View, California 94043</p></center>
<center><img src="Introduction/atclogo.jpg"></center>
<br>
<br>
<center><p>*Biological Systems Engineering<br>
Virginia Tech, Virginia 24061</p></center>
