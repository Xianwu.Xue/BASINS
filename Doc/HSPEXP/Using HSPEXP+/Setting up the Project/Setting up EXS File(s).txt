<h>Setting up EXS File(s)</h>


<p>HSPEXP+ uses the Basin Specification File (EXS file, extension *.exs) to obtain information about the calibration site, observed flow dataset, simulated datasets, drainage area, storm periods, error criteria, etc. The format of the EXS file is consistent with that used in HSPEXP. To read more about this file, please refer to the <a href="http://water.usgs.gov/software/HSPexp/code/doc/hspexp.pdf">HSPEXP manual</a> or refer to the EXS File information at the end of this section. HSPEXP+ currently does not have a capability to produce the EXS file. The EXS file can be used for calibration at one to ten separate locations.  If a user wants to calibrate at more than ten locations, or if the storm dates are different for different locations, the user can provide separate EXS file for each location. A user may have as many EXS files as needed.</p>

<p>When used with HSPEXP+, the EXS file can have comments, following the same format of a UCI file.  Any line with "***" is treated as a comment by HSPEXP+ (unlike its predecessor, HSPEXP).</p>

<p>As noted in the <a href="../../Introduction/Hydrologic Calibration">Hydrologic Calibration section</a>, HSPEXP+ considers May, June, and July to be the summer months, and December, January, February to be the winter months for calculating seasonal statistics.  In HSPEXP+, summer and winter months can be specified in the EXS file if needed, using the keyword "Seasons:", at the end of the EXS file.  Add a line with words, "Seasons:"; add a line with numbers for the summer months, separated by commas; and add a second line for the winter months separated by commas.<p>

<p>Description of all the parts of an EXS file are provided in the following table. Most of the information in this table is reproduced from the original HSPEXP manual (Page 34), with some changes that are relevant for HSPEXP+.</p>

<p><b>Basins-Specification File Content and Format</b></p>
<table style="width:100%" >
<tr>
<td> <b>Line Type/Content </b></td>
<td> <b>Number of Lines </b></td>
<td> <b>Line Format </b></td>
<td> <b>Description </b></td>
</tr>

<tr>
<td> General Information </td>
<td> 1 </td>
<td> A8, 2I5, 4F8, 2X, I4, 2I2, 2X, I4, 2I2</td>
<td> Name (prefix) of WDM file</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Number of Sites</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Current Site Number</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Latitude and longitude limits of the basins</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Start and end date for calibration statistics, if different from simulation period.</td>
</tr>

<tr>
<td>WDM Data Set ID#'s</td>
<td># of sites</td>
<td>10I4, 1X, I2, 2X, A20</td>
<td>DSN for simulated runoff (inches)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>DSN for observed flow (cfs)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>DSN for simulated surface runoff (inches)</td>
</tr>


<tr>
<td></td>
<td></td>
<td></td>
<td>DSN for simulated interflow (inches)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>DSN for simulated baseflow (inches)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>DSN for precipitation (inches)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>DSN for potential evapotranspiration (inches)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>DSN for actual evapotranspiration (inches)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>DSN for upper zone storage (inches)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>DSN for lower zone storage (inches)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Flag indicating where statistics have been calculated for the site (not used)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Site Name</td>
</tr>

<tr>
<td>Number of Storms</td>
<td># of storms</td>
<td>I4</td>
<td>Number of storm periods</td>
</tr>

<tr>
<td>Storm periods</td>
<td># of storms</td>
<td>I5, 5I3, I5, 5I3</td>
<td>Storm start and end date/time (yr, mo, dy, hr, min, sec)</td>
</tr>

<tr>
<td>Drainage Areas</td>
<td>1</td>
<td>10F8</td>
<td>Drainage area (acres) for each site</td>
</tr>

<tr>
<td>Error Criteria</td>
<td># of sites</td>
<td>10F8</td>
<td>Values for error terms and criteria</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Acceptable error in total volume (%)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Acceptable error in low flow recession (-)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Acceptable error in 50% lowest flows (%)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Acceptable error in 10% highest flows (%)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Ratio of interflow to surface runoff</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Acceptable error in seasonal volumes (%)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Acceptable error in summer storm volumes (%)</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Multiplier on third and fourth error terms</td>
</tr>

<tr>
<td></td>
<td></td>
<td></td>
<td>Percent of flows to use on low-flow recession error</td>
</tr>

<tr>
<td>Seasons keyword</td>
<td>1</td>
<td>A7</td>
<td>Enter the word "Seasons:"</td>
</tr>

<tr>
<td>Summer months</td>
<td>1</td>
<td>csv</td>
<td>Enter the months for summer in csv format</td>
</tr>

<tr>
<td>Winter months</td>
<td>1</td>
<td>csv</td>
<td>Enter the months for winter in csv format</td>
</tr>

<tr>
<td>EXS file notes:</td>
<td></td>
<td></td>
<td></td>
</tr>

<tr>
<td>Line formats</td>
<td></td>
<td></td>
<td></td>
</tr>

</table>
<br>
<table style="width:100%">
<tr>
<td></td>
<td>A<i>n</i> - character input with column width = n columns </td>
</tr>

<tr>
<td></td>
<td>I<i>n</i> - integer input with column width = n columns </td>
</tr>


<tr>
<td></td>
<td>F<i>n</i> - real number input with column width = n columns </td>
</tr>

<tr>
<td></td>
<td><i>n</i>X - n blank spaces </td>
</tr>

<tr>
<td></td>
<td>csv - comma-separated values </td>
</tr>

</table>

<table style="width:100%">
<tr>
<td> Unneeded lines - the last 5 line types in an EXS file used with the original HSPEXP program are not used on HSPEXP+; these are 3 lines of computed statistics and 2 lines of flags for ancillary data
</td>
</tr>
</table>
<hr>

<p>An example EXS file is shown below. Please refer to HSPEXP manual (page 34) for details about the rest of the format.</p>
<center><img src="ExampleEXSFile.png"></center>

















